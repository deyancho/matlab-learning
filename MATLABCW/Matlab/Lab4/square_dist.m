function sq_dist = square_dist(U,v)
%Compute 1xM row vector of square distances for MxN and 1xN data
% U and v, respectively.
    sq_dist = sum(bsxfun(@minus, U,v).^2,2)';
end