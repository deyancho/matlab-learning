function myKMeans(structureArray,numClust,maxiter)
% TO BE CONTINUED
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    %Check the boundaries of the data
    
    A = structureArray.data;
    col_headers = structureArray.colheaders;
    [numObserv, vectDim] = size(A);
    minVals = min(A);
    maxVals = max(A);
    minBound = minVals; 
    maxBound = maxVals;
    Dist = zeros(numClust, numObserv);
    
    %Initialize random cluster centres
    zeroMatrCentres = zeros(numClust,vectDim)
    for c = 1:numClust
        for k = 1:vectDim
            zeroMatrCentres(c,k) = minBound(1,k) + (maxBound(1,k)-minBound(1,k))*rand(1,1);
        end
    end
    centers = zeroMatrCentres %change to meaningfulname, pay speed cost
    
    % Cell array of different MATLAB color points needed if vectDim <= 3
    colors = distinguishable_colors(numClust);
    
    if (vectDim==2)
        fprintf('[0]Iteration: ');
        scatter(A(:,1), A(:,2),'+');
        xlabel(col_headers{1});
        ylabel(col_headers{2});
        title((sprintf('Iteration %i',0)));
        for c = 1:numClust
            hold on;
            scatter(centers(c,1),centers(c,2),30,colors(c,:),'filled');
        end
        for i = 1:maxiter
            for m = 1:numClust
                Dist(m,:) = square_dist(A,centers(m,:));
            end
            prev_centres = centers;
            [Ds, idx] = min(Dist);
            for l = 1:numClust
                if (sum(idx==c)==0)
                    warn('k-means: cluster %d is empty', l);
                else
                    centers(c,:) = mean(A(idx == l,:));
                end
            end
            if (prev_centres == centers)
                break;
            end
            
            centers
            figure
            
            for count = 1:numClust
                scatter(centers(count,1),centers(count,2),30, colors(count,:),'filled')
                hold on;
            end
            
            for o = 1:numClust
                matr = A(idx==o,:);
                scatter(matr(:,1),matr(:,2),30,colors(o,:),'+')
                hold on;
            end
            xlabel(col_headers{1});
            ylabel(col_headers{2});
            title((sprintf('Iteration %i',i)));
        end
    end


end

