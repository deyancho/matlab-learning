centres = [2 90; 5 50];
K = size(centres,1);
[N dim] = size(A);
maxiter = 100;
D = zeros(K,N);
fprintf('[0]Iteration: ')
centres
scatter(A(:,1), A(:,2),'+')
xlabel(col_headers{1});
ylabel(col_headers{2});
title((sprintf('Iteration %i',0)));
for c = 1:K
    hold on;
    scatter(Cc0(c,1),Cc0(c,2),300,colors{c});
end

for i = 1:maxiter
    for c = 1:K
        D(c,:) = square_dist(A,centres(c,:));
        
    end
    prev_centres = centres;
    [Ds, idx] = min(D);
    for c = 1:K
        if (sum(idx==c)==0)
            warn('k-means: cluster %d is empty', c);
        else
            centres(c,:) = mean(A(idx == c,:));
        end
    end
    if (prev_centres == centres)
        break;
    end
    fprintf('[%d] Iteration: ', i)
    centres
    
    figure;
    for c = 1:K
        
        matr = A(idx==c,:);
        scatter(matr(:,1),matr(:,2),'+', colors{c+2})
        hold on;
    end
    
    for count = 1:K
        scatter(centres(count,1),centres(count,2),300, colors{count})
        hold on;
    end
    xlabel(col_headers{1});
    ylabel(col_headers{2});
    title((sprintf('Iteration %i',i)));
        