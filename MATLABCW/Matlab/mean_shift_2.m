function [ mean_matrix ] = mean_shift_2( matrix )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
    [I,J] = size(matrix);
    mu = mean(matrix,1);
    mean_matrix = matrix;
    for i = 1:I
        mean_matrix(i,:) = mean_matrix(i,:) - mu;
    end
    



end

