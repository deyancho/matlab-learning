function [ A_shift ] = mean_shift_1( A )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
    [I , J] = size(A);
    mu = mean(A,1);
    A_shift = A;
    for i= 1:I
        for j = 1:J
            A_shift(i,j) = A_shift(i,j)-mu(j);
        end
    end
end

