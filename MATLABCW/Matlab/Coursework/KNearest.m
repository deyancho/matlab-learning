function [ testLabels ] = KNearest(k,testData,trainedDataLab,Data)
    
    % The KNearest function takes as input
    % the number of neighbours, the data to be labeled
    % (testData), some data(Data), and the labels of that trained
    % data and returns the labels of the test data.
    format long;
    %Minimum value of the classes that are labeling the trained data.
    minDataLab = min(trainedDataLab)
    %Maximum val.
    maxDataLab = max(trainedDataLab)
    [rowsTest,colsTest] = size(testData);
    [rowsTrain,colsTrain] = size(Data);
    %We'll store the labels here.
    testLabels = zeros(1,rowsTest);
    %We'll store the distances between each point here:
    %The (i,j)-th entry corresponds to the distance between
    %the i-th test point and the j-th trained point.
    distancesMatrix = zeros(rowsTest,rowsTrain);
    for i = 1:rowsTest
        distancesMatrix(i,:) = square_dist(Data,testData(i,:));
    end
    [sortedMatrix,indicesMatrix] = sort(distancesMatrix,2,'ascend')
    necessaryIndices = indicesMatrix(:,(1:1:k))
    [necIndRow,necIndCol] = size(necessaryIndices)
    
    necessaryClassLabels = trainedDataLab(necessaryIndices)
    
    % The binranges may depend on the min value and max value of the classes
    % here it's from 1 to 10.
    binranges = [minDataLab:maxDataLab];
    % the frequency matrix for the different classes of the nearest points
    % the 1st row is the class, the 2nd is its frequency as a nearest neighbour.
    for count = 1:rowsTest
        frequencyMatr = zeros(2,10);
        frequencyMatr = histc(necessaryClassLabels(count,:),binranges)
        maxClassOccurences = max(frequencyMatr(1,:))
        indicesOfMax = find(frequencyMatr == maxClassOccurences)
        if length(indicesOfMax)==1
            testLabels(1,rowsTest) = minDataLab + indicesOfMax(1,1)-1;
        end
        if length(indicesOfMax)>1
            for count2 = 1:necIndCol
                for count3 = 1:length(indicesOfMax)
                    if necessaryIndices(count,count2) == minDataLab + indicesOfMax(1,count3)-1
                        testLabels(1,count) = necessaryIndices(count,count2)
                    end
                end
            end
        end
    end
     
end

