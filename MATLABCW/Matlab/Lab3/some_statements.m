%Critic reviews vector space

x = A(:,2); %x-axis
y = A(:,3); %y-axis
z = A(:,6); %z-axis

% Create a scatter3 plot with 'filled points'
scatter3(x,y,z, 'filled')
axis([-1, 11, -1, 11, -1, 11]);
% Set the x,y and z axis limits

% Set the x, y and z labels
xlabel('Movie 2');
ylabel('Movie 3');
zlabel('Movie 6');




