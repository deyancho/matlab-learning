tic
apply_pca;
%Need to compute the twoDData and the twoDData projected onto the first PCs
twoDData = X_PCA;
classes = unique(train_classes');
twoDTest = test_features*E2;

[Xv Yv] = meshgrid((min(twoDData(:,1))-5):0.05:(max(twoDData(:,1))+5),(min(twoDData(:,2))-5):0.05:(max(twoDData(:,2))+5));
gridbla = [Xv(:), Yv(:)];

plotDecisionBoundaries(twoDTest,test_classes,{'1','2','3','4','5','6','7','8','9','0'},twoDData,1,train_classes);
toc