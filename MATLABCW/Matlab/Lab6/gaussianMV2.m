function y = gaussianMV2(mu,covar,X)

    [n,d] = size(X);
    [j,k] = size(covar);
    
    if ((j~=d)|(k~=d))
       error('Dimension of covariance matrix and data should match'); 
    end

    invcov = inv(covar);
    mu = reshape(mu,1,d);
    X = X-ones(n,1)*mu;
    fact = sum(((X*invcov).*X),2);
    y = exp(-0.5*fact);
    y = y./sqrt((2*pi)^d*det(covar));
    
end