function [EVecs,Evals] = compute_pca( X )
    format long;
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
    NumPoints = length(X);
    featuresMean = MyMean(X);
    X = bsxfun(@minus,X,featuresMean);
    covar_X = 1/(NumPoints-1)*(X'*X);
    [PrincipleComp,eigenvalues] = eig(covar_X);
    eigenvalues = diag(eigenvalues);
    [Evals,indexOfValsVect] = sort(eigenvalues,1,'descend');
    PrincipleComp = PrincipleComp(:,indexOfValsVect);
    dimEigMatrix = size(X,2);
    for i = 1:dimEigMatrix
        if PrincipleComp(1,i)<0
            PrincipleComp(:,i) = PrincipleComp(:,i)*-1;
        end
    end
    EVecs = normc(PrincipleComp);
end

