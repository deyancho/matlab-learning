[EVecs,Evals] = compute_pca(train_features);
XnumDims = zeros(1,100);
for dim = 1:100
    numDims(1,dim) = dim;
end

YCumVar = zeros(1,100);

for dim = 1:100
    for sumCum = 1:dim
        YCumVar(1,dim) = YCumVar(1,dim)+Evals(sumCum,1);
    end
end
figure
plot(numDims,YCumVar)
xlabel('Number of dimensions x')
ylabel('Cumulative variance for x')