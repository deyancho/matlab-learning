% This code uses my written function KNearest.m for k = 1. This could 
% easily be changed by changing the value of the first parameter of the
% KNearest function to whatever.

testLabels = KNearest(5,test_features,train_classes,train_features);
KNearestTest(5,test_features,train_classes,train_features);
testLabels = uint8(testLabels');
sprintf('The confusion matrix for all dimensions is:')
confusionM = computeConf(test_classes,testLabels)
confusionDiagonal = diag(confusionM);
numCorrectClassifications = sum(confusionDiagonal)
allClassifications = sum(sum(confusionM))
correctClassificationRate = numCorrectClassifications/allClassifications
