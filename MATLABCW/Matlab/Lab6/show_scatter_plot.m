figure
apply_pca;
%Please wait for a couple of minutes
%until all data points are plotted.
%The code is not the most efficient.
%Also,apply_pca.m must be run before this code can be run.

my_color1 = [255 192 56] ./ 255;
my_color2 = [33 74 212] ./ 255;
my_color3 = [17 192 14] ./ 255;
my_color4 = [230 47 69] ./ 255;
my_color5 = [124 171 40] ./ 255;
my_color6 = [95 92 24] ./ 255;
my_color7 = [14 34 57] ./ 255;
my_color8 = [61 88 113] ./ 255;
my_color9 = [5 113 134] ./ 255;
my_color10 = [121 98 227] ./ 255;
colors = {my_color1,my_color2,my_color3,my_color4,my_color5,my_color6,my_color7,my_color8,my_color9,my_color10};

for points = 1:10000
    scatter(X_PCA(points,1),X_PCA(points,2),30,colors{train_classes(1,points)},'.');
    hold on;
end

xlabel('1st Principal Component');
ylabel('2nd Principal Component');

axis([-30 26 -20 32]);
box on;