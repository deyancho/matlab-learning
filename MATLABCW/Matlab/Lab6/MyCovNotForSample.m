function [covMat] = MyCovNotForSample(X)
    N = length(X);
    x_mean = MyMean(X);
    X = bsxfun(@minus,X,x_mean);
    covMat = 1/(N)*(X'*X);

end