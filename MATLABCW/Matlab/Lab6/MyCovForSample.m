function [covMat] = MyCovForSample(X)
    N = length(X);
    x_mean = MyMean(X);
    X = bsxfun(@minus,X,x_mean);
    covMat = 1/(N-1)*(X'*X);

end