%Compute the shared covariance
%as the covariance of all training data points.

sharedCov = (covMatCl1+covMatCl2+covMatCl3+covMatCl4+covMatCl5+covMatCl6+covMatCl7+covMatCl8+covMatCl9+covMatCl10)/10;

deterSharedCov = det(sharedCov)


%Get the unique class labels.
uniqueClassLabelslda = unique(train_classes');

%(lots of copy-pasting, not a good way to write code but whatever)

%Get the indices for the different classes
class1Indiceslda = (find(train_classes' == uniqueClassLabels(1)));
class2Indiceslda = (find(train_classes' == uniqueClassLabels(2)));
class3Indiceslda = (find(train_classes' == uniqueClassLabels(3)));
class4Indiceslda = (find(train_classes' == uniqueClassLabels(4)));
class5Indiceslda = (find(train_classes' == uniqueClassLabels(5)));
class6Indiceslda = (find(train_classes' == uniqueClassLabels(6)));
class7Indiceslda = (find(train_classes' == uniqueClassLabels(7)));
class8Indiceslda = (find(train_classes' == uniqueClassLabels(8)));
class9Indiceslda = (find(train_classes' == uniqueClassLabels(9)));
class10Indiceslda = (find(train_classes' == uniqueClassLabels(10)));

%Extract the data from the different classes
dataClass1lda = train_features(class1Indices,:);
dataClass2lda = train_features(class2Indices,:);
dataClass3lda = train_features(class3Indices,:);
dataClass4lda = train_features(class4Indices,:);
dataClass5lda = train_features(class5Indices,:);
dataClass6lda = train_features(class6Indices,:);
dataClass7lda = train_features(class7Indices,:);
dataClass8lda = train_features(class8Indices,:);
dataClass9lda = train_features(class9Indices,:);
dataClass10lda = train_features(class10Indices,:);

%Get the mean vector for each class
meanClass1lda = MyMean(dataClass1);
meanClass2lda = MyMean(dataClass2);
meanClass3lda = MyMean(dataClass3);
meanClass4lda = MyMean(dataClass4);
meanClass5lda = MyMean(dataClass5);
meanClass6lda = MyMean(dataClass6);
meanClass7lda = MyMean(dataClass7);
meanClass8lda = MyMean(dataClass8);
meanClass9lda = MyMean(dataClass9);
meanClass10lda = MyMean(dataClass10);

likelihoodsCllda = gaussianMV2(meanClass1lda,sharedCov,test_features);
likelihoodsCl2lda = gaussianMV2(meanClass2lda,sharedCov,test_features);
likelihoodsCl3lda = gaussianMV2(meanClass3lda,sharedCov,test_features);
likelihoodsCl4lda = gaussianMV2(meanClass4lda,sharedCov,test_features);
likelihoodsCl5lda = gaussianMV2(meanClass5lda,sharedCov,test_features);
likelihoodsCl6lda = gaussianMV2(meanClass6lda,sharedCov,test_features);
likelihoodsCl7lda = gaussianMV2(meanClass7lda,sharedCov,test_features);
likelihoodsCl8lda = gaussianMV2(meanClass8lda,sharedCov,test_features);
likelihoodsCl9lda = gaussianMV2(meanClass9lda,sharedCov,test_features);
likelihoodsCl10lda = gaussianMV2(meanClass10lda,sharedCov,test_features);


%I'm not really sure what we're supposed to do here
%we could either take the log of these pdfs and take
%that as our discriminant functions or we could 
%reduce algebraically to lda = mu(transpose)*sigmainverse*x -
%1/2*mu(transpose)*sigmaInverse*mu (i'll drop the priors because they're
%equal). I'll do the former for the time being, as the two procedures are equivalent.

ldf1 = log(likelihoodsCllda);
ldf2 = log(likelihoodsCl2lda);
ldf3 = log(likelihoodsCl3lda);
ldf4 = log(likelihoodsCl4lda);
ldf5 = log(likelihoodsCl5lda);
ldf6 = log(likelihoodsCl6lda);
ldf7 = log(likelihoodsCl7lda);
ldf8 = log(likelihoodsCl8lda);
ldf9 = log(likelihoodsCl9lda);
ldf10 = log(likelihoodsCl10lda);


allProbsAllPointslda = [ldf1,ldf2,ldf3,ldf4,ldf5,ldf6,ldf7,ldf8,ldf9,ldf10]
[maxProbslda,indicesMaxlda] = max(allProbsAllPointslda,[],2)

confusionGaussianlda = computeConf(test_classes,indicesMaxlda)
sumCorrectGaussianlda = sum(diag(confusionGaussianlda))
allClassificationsGaussianlda = sum(sum(confusionGaussianlda))
accuracyRateGaussianlda = sumCorrectGaussianlda/allClassificationsGaussianlda

sharedCovInv = inv(sharedCov);

probabilitiesByYofX = zeros(1000,10);
for testPoint = 1:1000
        probabilitiesByYofX(testPoint,:) = [(meanClass1lda*(sharedCov\(test_features(testPoint,:))'))-(0.5*meanClass1lda*(sharedCov\meanClass1lda')),(meanClass2lda*(sharedCov\(test_features(testPoint,:))'))-(1/2*meanClass2lda*(sharedCov\meanClass2lda')),(meanClass3lda*(sharedCov\(test_features(testPoint,:))'))-(1/2*meanClass3lda*(sharedCov\meanClass3lda')),(meanClass4lda*(sharedCov\(test_features(testPoint,:))'))-(1/2*meanClass4lda*(sharedCov\meanClass4lda')),(meanClass5lda*(sharedCov\(test_features(testPoint,:))'))-(1/2*meanClass5lda*(sharedCov\meanClass5lda')),(meanClass6lda*(sharedCov\(test_features(testPoint,:))'))-(1/2*meanClass6lda*(sharedCov\meanClass6lda')),(meanClass7lda*(sharedCov\(test_features(testPoint,:))'))-(1/2*meanClass7lda*(sharedCov\meanClass7lda')),(meanClass8lda*(sharedCov\(test_features(testPoint,:))'))-(1/2*meanClass8lda*(sharedCov\meanClass8lda')),(meanClass9lda*(sharedCov\(test_features(testPoint,:))'))-(1/2*meanClass9lda*(sharedCov\meanClass9lda')),(meanClass10lda*(sharedCov\(test_features(testPoint,:))'))-(1/2*meanClass10lda*(sharedCov\meanClass10lda'))];
end

[maxProbslda2,indicesMaxlda2] = max(probabilitiesByYofX,[],2)

confusionGaussianlda2 = computeConf(test_classes,indicesMaxlda2)
sumCorrectGaussianlda2 = sum(diag(confusionGaussianlda2))
allClassificationsGaussianlda2 = sum(sum(confusionGaussianlda2))
accuracyRateGaussianlda2 = sumCorrectGaussianlda2/allClassificationsGaussianlda2
