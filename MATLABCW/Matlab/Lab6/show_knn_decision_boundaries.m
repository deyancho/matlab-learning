apply_pca
twoDData = X_PCA;
classes = unique(train_classes')

[Xv Yv] = meshgrid((min(twoDData(:,1))-5):0.05:(max(twoDData(:,1))+5),(min(twoDData(:,2))-5):0.05:(max(twoDData(:,2))+5));
%The code takes a lot of time to run. About 15 minutes to show the graph.
%The colors at the center appear black because the decision boundary is black
%the points are clustered at a very small space and decision boundary is interfering.

my_color1 = [255 192 56] ./ 255; %Color for class 1.
my_color2 = [33 74 212] ./ 255; %For class 2 etc.
my_color3 = [17 192 14] ./ 255;
my_color4 = [230 47 69] ./ 255;
my_color5 = [124 171 40] ./ 255;
my_color6 = [95 92 24] ./ 255;
my_color7 = [14 34 57] ./ 255;
my_color8 = [61 88 113] ./ 255;
my_color9 = [5 113 134] ./ 255;
my_color10 = [121 98 227] ./ 255;
colors = {my_color1,my_color2,my_color3,my_color4,my_color5,my_color6,my_color7,my_color8,my_color9,my_color10};
classes2 = zeros(size(Xv));

%The code below will plot the training set colored appropriately
%with a black decision boundary line.

%Compute the first two principle component coordinates for the test data.

twoDTest = test_features*E2;
for count = 1:1000
    scatter(twoDTest(count,1),twoDTest(count,2),60,colors{test_classes(1,count)},'.');
    hold on;
end


for i = 1:length(Xv(:))
    point = [Xv(i) Yv(i)];
    classes2(i) = KNearest(1,point,train_classes,twoDData);
end

figure(1);
hold off;
for i = 1:length(classes)
    sth = (find(train_classes' == classes(i)))';
    scatter((twoDData(sth,1)),(twoDData(sth,2)),30,colors{i},'.');
    xlabel('First PC');
    ylabel('Second PC');
    hold on;
end
axis([-30 26 -20 32]);
contour(Xv,Yv,classes2,'LineColor',[0 0 0])
    
%Now on the graph plot the test data. The test points 
%will be colored according to their classes.
%The points will be noticable because they will be bigger than the training set
%points.



    


