function [mean] = MyMean(X)
    
    %Get the mean of each column.
    [rows, cols] = size(X);
    mean = zeros(1,cols);
    for count1 = 1:cols
        for count2 = 1:rows
            mean(1,count1) = mean(1,count1)+X(count2,count1);
        end
    end
    mean = mean/rows;

end
