function KNearestTest(k,testData,trainLabels,trainData)
        vect1 = KNearest(k,testData,trainLabels,trainData);
        group = trainLabels';
        vect2 = knnclassify(testData,trainData,group,k,'euclidean','nearest');
        i = 1;
        if length(vect1) ~= length(vect2)
            i = 0;
        end
        if length(vect1) == length(vect2)
            for m = 1:length(vect1)
                if vect1(m,1) ~= vect2(m,1)
                    sprintf('mismatch at point %i',m)
                    i = 0;
                    
                end
            end
        end
        
        if i == 1
            sprintf('Test for KNearest passed, no mismatch between built-in function knnclassify and KNearest.')
        end
        
            
            



end