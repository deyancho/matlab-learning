function [W] = gradientDescentTraining(X,T,W,learnRate,maxIterations)
        %Takes W as a K by d+1 matrix where K is the number of the classes
        %and d+1 is the dimension of the data + one bias weight at the
        %beginning of every row to account for the bias
        %X is the data matrix where each column is a test point each
        %row is a dimension where the first dimension = 1 to account for
        %the bias, i.e we have a (d+1)x N matrix.
        
        %Maximum number of iterations before the algorithm stops
        maxIter = maxIterations;
       
        [weightRows,weightCols] = size(W)
        [trainingRows,trainingCols] = size(X)
        
        %Allocate space for the output matrix
        outputMatrix = zeros(weightRows,trainingCols)
        
        %Allocate space for the error signals.
        
        errorSignals = zeros(weightRows,trainingCols)
        
        %Initialize a change in biases matrix, initially all zeroes.
        
        iterNum = 1;
        while(iterNum<=maxIter)
            changeInBiases = zeros(weightRows,weightCols)
            for trainingPoint = 1:trainingCols
                for classNum = 1:weightRows
                    outputMatrix(classNum,trainingPoint) = W(classNum,:)*X(:,trainingPoint)
                    errorSignals(classNum,trainingPoint) = outputMatrix(classNum,trainingPoint) - T(classNum,trainingPoint)
                    for dim = 1:trainingRows
                        changeInBiases(classNum,dim) = changeInBiases(classNum,dim)+errorSignals(classNum,trainingPoint)*X(dim,trainingPoint)
                    end
                end
            end
            for classRow = 1:weightRows
                for classDimRow = 1:weightCols
                    W(classRow,classDimRow) = W(classRow,classDimRow) - learnRate*changeInBiases(classRow,classDimRow)
                end
            end
            iterNum = iterNum+1;
        end
                    
end