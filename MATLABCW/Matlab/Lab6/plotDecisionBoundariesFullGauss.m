function plotDecisionBoundariesFullGauss(X, true_labels, label_names, trainingSet,trueLabelsTrain)
   %Inputs:
%    X : your data to be visualized, N x 2, where N is the number of datapoints you are plotting.
%    true_labels : N sized vector of the true classes of the N data points 
%    label_names : Cell array with the name of the labels, i.e. {'1', '2', '3', ..., '0'}
%    classifier : This is the function that you've programmed to classify. It can be k-NN, or any of the two Gaussian classifiers. Note that you can add arguments to plotDecisionBoundaries if you classifier function needs more arguments or adapt it as you want.
%     
    
%   We define here the colormap we will use to colour each of the 10 classes.
    cmap = [0.80369089,  0.61814689,  0.46674357;
        0.81411766,  0.58274512,  0.54901962;
        0.58339103,  0.62000771,  0.79337179;
        0.83529413,  0.5584314 ,  0.77098041;
        0.77493273,  0.69831605,  0.54108421;
        0.72078433,  0.84784315,  0.30039217;
        0.96988851,  0.85064207,  0.19683199;
        0.93882353,  0.80156864,  0.4219608 ;
        0.83652442,  0.74771243,  0.61853136;
        0.7019608 ,  0.7019608 ,  0.7019608];

    % Stepsize defines how fine-grained we want our grid. The small the
    % value, the more resolution the visualization will have, at the
    % expense of computational cost (we would need to classify more
    % data-points since the grid would be denser).
    stepSize = 0.05;
    
    x1range = (max(X(:,1)) +12- min(X(:,1)));
    x2range = (max(X(:,2))+12 - min(X(:,2)));
    
    x1plot = linspace(min(X(:,1))-6, max(X(:,1))+6, x1range/stepSize)';
    x2plot = linspace(min(X(:,2))-6, max(X(:,2))+6, x2range/stepSize)';
    
    % We obtain the grid vectors for the two dimensions.    
    [X1, X2] = meshgrid(x1plot, x2plot);
    
    % Concatenate them such that we can feed 'gridX' to your classifier.
    gridX = [X1(:), X2(:)]
    
    % Call here your classification method using the function you've coded to obtain the labels for each point
    % in the grid. Adapt this to you code!:
    [row col] = size(gridX)
    
    %Get the unique class labels.
    uniqueClassLabelsplot = unique(trueLabelsTrain');

    %(lots of copy-pasting, not a good way to write code but whatever)

    %Get the indices for the different classes
    class1Indicesplot = (find(trueLabelsTrain' == uniqueClassLabelsplot(1)));
    class2Indicesplot = (find(trueLabelsTrain' == uniqueClassLabelsplot(2)));
    class3Indicesplot = (find(trueLabelsTrain' == uniqueClassLabelsplot(3)));
    class4Indicesplot = (find(trueLabelsTrain' == uniqueClassLabelsplot(4)));
    class5Indicesplot = (find(trueLabelsTrain' == uniqueClassLabelsplot(5)));
    class6Indicesplot = (find(trueLabelsTrain' == uniqueClassLabelsplot(6)));
    class7Indicesplot = (find(trueLabelsTrain' == uniqueClassLabelsplot(7)));
    class8Indicesplot = (find(trueLabelsTrain' == uniqueClassLabelsplot(8)));
    class9Indicesplot = (find(trueLabelsTrain' == uniqueClassLabelsplot(9)));
    class10Indicesplot = (find(trueLabelsTrain' == uniqueClassLabelsplot(10)));

    %Extract the data from the different classes
    dataClass1plot = trainingSet(class1Indicesplot,:);
    dataClass2plot = trainingSet(class2Indicesplot,:);
    dataClass3plot = trainingSet(class3Indicesplot,:);
    dataClass4plot = trainingSet(class4Indicesplot,:);
    dataClass5plot = trainingSet(class5Indicesplot,:);
    dataClass6plot = trainingSet(class6Indicesplot,:);
    dataClass7plot = trainingSet(class7Indicesplot,:);
    dataClass8plot = trainingSet(class8Indicesplot,:);
    dataClass9plot = trainingSet(class9Indicesplot,:);
    dataClass10plot = trainingSet(class10Indicesplot,:);

    %Get the mean vector for each class
    meanClass1plot = MyMean(dataClass1plot);
    meanClass2plot = MyMean(dataClass2plot);
    meanClass3plot = MyMean(dataClass3plot);
    meanClass4plot = MyMean(dataClass4plot);
    meanClass5plot = MyMean(dataClass5plot);
    meanClass6plot = MyMean(dataClass6plot);
    meanClass7plot = MyMean(dataClass7plot);
    meanClass8plot = MyMean(dataClass8plot);
    meanClass9plot = MyMean(dataClass9plot);
    meanClass10plot = MyMean(dataClass10plot);

    %The MyCovNotForSample computes the 
    %means again which is inefficient, I'll that for now.

    covMatCl1plot = MyCovNotForSample(dataClass1plot)
    covMatCl2plot = MyCovNotForSample(dataClass2plot)
    covMatCl3plot = MyCovNotForSample(dataClass3plot)
    covMatCl4plot = MyCovNotForSample(dataClass4plot)
    covMatCl5plot = MyCovNotForSample(dataClass5plot)
    covMatCl6plot = MyCovNotForSample(dataClass6plot)
    covMatCl7plot = MyCovNotForSample(dataClass7plot)
    covMatCl8plot = MyCovNotForSample(dataClass8plot)
    covMatCl9plot = MyCovNotForSample(dataClass9plot)
    covMatCl10plot = MyCovNotForSample(dataClass10plot)


    likelihoodsCl1plot = gaussianMV2(meanClass1plot,covMatCl1plot,gridX);
    likelihoodsCl2plot = gaussianMV2(meanClass2plot,covMatCl2plot,gridX);
    likelihoodsCl3plot = gaussianMV2(meanClass3plot,covMatCl3plot,gridX);
    likelihoodsCl4plot = gaussianMV2(meanClass4plot,covMatCl4plot,gridX);
    likelihoodsCl5plot = gaussianMV2(meanClass5plot,covMatCl5plot,gridX);
    likelihoodsCl6plot = gaussianMV2(meanClass6plot,covMatCl6plot,gridX);
    likelihoodsCl7plot = gaussianMV2(meanClass7plot,covMatCl7plot,gridX);
    likelihoodsCl8plot = gaussianMV2(meanClass8plot,covMatCl8plot,gridX);
    likelihoodsCl9plot = gaussianMV2(meanClass9plot,covMatCl9plot,gridX);
    likelihoodsCl10plot = gaussianMV2(meanClass10plot,covMatCl10plot,gridX);


    allProbsAllPointsplot = [likelihoodsCl1plot,likelihoodsCl2plot,likelihoodsCl3plot,likelihoodsCl4plot,likelihoodsCl5plot,likelihoodsCl6plot,likelihoodsCl7plot,likelihoodsCl8plot,likelihoodsCl9plot,likelihoodsCl10plot];
    [maxProbsplot,grid_labels] = max(allProbsAllPointsplot,[],2);
    grid_labels = grid_labels';
    
    % Now we create the figure to visualize:
    figure;
    
    % This function will draw the decision boundaries
    [C,h] = contourf(x1plot(:), x2plot(:), reshape(grid_labels, length(x2plot),length(x1plot)));
    set(h,'LineColor','none')

    % Important calls to properly define the color map:
    colormap(cmap);

    % Range of our class labels for the color mapping.
    caxis([1 10]);
    
    hold on;
    
    % Plot the scatter plots grouped by their classes, with black border.
    scatters = gscatter(X(:,1),X(:,2),true_labels, [0,0,0], 'o', 10);
    
    % Fill in the color of each point according to the class labels.
    for n = 1:length(scatters)
      set(scatters(n), 'MarkerFaceColor', cmap(n,:));
    end
    
    legend(scatters,label_names);
  
    hold off;
end