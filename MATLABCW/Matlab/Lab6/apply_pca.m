X = train_features;
[newBases,eigenvals] = compute_pca(X);
E2 = newBases(:,[1,2]);
X_PCA = X*E2;
% For the report
largest2Vals = eigenvals([1,2],:)
firstFiveE2 = E2([1,2,3,4,5],:)
firstFiveXPCA = X_PCA([1,2,3,4,5],:)
