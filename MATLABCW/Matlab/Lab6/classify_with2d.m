apply_pca;
twoDData = X_PCA;
twoDTest = test_features*E2;
testLabels2 = KNearest(1,twoDTest,train_classes,X_PCA);
testLabels2 = uint8(testLabels2');
sprintf('The confusion matrix for first 2 PCs is:')
confusionM2dknn = computeConf(test_classes,testLabels2)
confusionDiagonal2dknn = diag(confusionM2dknn)
numCorrectClassifications2 = sum(confusionDiagonal2dknn)
allClassifications2dknn = sum(sum(confusionM2dknn))
correctClassificationRate2dknn = numCorrectClassifications2/allClassifications2dknn



uniqueClassLabels = unique(train_classes');

%(lots of copy-pasting, not a good way to write code but whatever)

%Get the indices for the different classes
class1Indices2d = (find(train_classes' == uniqueClassLabels(1)));
class2Indices2d = (find(train_classes' == uniqueClassLabels(2)));
class3Indices2d = (find(train_classes' == uniqueClassLabels(3)));
class4Indices2d = (find(train_classes' == uniqueClassLabels(4)));
class5Indices2d = (find(train_classes' == uniqueClassLabels(5)));
class6Indices2d = (find(train_classes' == uniqueClassLabels(6)));
class7Indices2d = (find(train_classes' == uniqueClassLabels(7)));
class8Indices2d = (find(train_classes' == uniqueClassLabels(8)));
class9Indices2d = (find(train_classes' == uniqueClassLabels(9)));
class10Indices2d = (find(train_classes' == uniqueClassLabels(10)));

%Extract the data from the different classes
dataClass12d = twoDData(class1Indices2d,:);
dataClass22d = twoDData(class2Indices2d,:);
dataClass32d = twoDData(class3Indices2d,:);
dataClass42d = twoDData(class4Indices2d,:);
dataClass52d = twoDData(class5Indices2d,:);
dataClass62d = twoDData(class6Indices2d,:);
dataClass72d = twoDData(class7Indices2d,:);
dataClass82d = twoDData(class8Indices2d,:);
dataClass92d = twoDData(class9Indices2d,:);
dataClass102d = twoDData(class10Indices2d,:);

%Get the mean vector for each class
meanClass12d = MyMean(dataClass12d);
meanClass22d = MyMean(dataClass22d);
meanClass32d = MyMean(dataClass32d);
meanClass42d = MyMean(dataClass42d);
meanClass52d = MyMean(dataClass52d);
meanClass62d = MyMean(dataClass62d);
meanClass72d = MyMean(dataClass72d);
meanClass82d = MyMean(dataClass82d);
meanClass92d = MyMean(dataClass92d);
meanClass102d = MyMean(dataClass102d);

%The MyCovNotForSample computes the 
%means again which is inefficient, I'll that for now.

covMatCl12d = MyCovNotForSample(dataClass12d);
covMatCl22d = MyCovNotForSample(dataClass22d);
covMatCl32d = MyCovNotForSample(dataClass32d);
covMatCl42d = MyCovNotForSample(dataClass42d);
covMatCl52d = MyCovNotForSample(dataClass52d);
covMatCl62d = MyCovNotForSample(dataClass62d);
covMatCl72d = MyCovNotForSample(dataClass72d);
covMatCl82d = MyCovNotForSample(dataClass82d);
covMatCl92d = MyCovNotForSample(dataClass92d);
covMatCl102d = MyCovNotForSample(dataClass102d);

covMatShared2d = (covMatCl12d + covMatCl22d + covMatCl32d + covMatCl42d + covMatCl52d + covMatCl62d + covMatCl72d + covMatCl82d + covMatCl92d + covMatCl102d)/10;


likelihoodsCl12d = gaussianMV2(meanClass12d,covMatCl12d,twoDTest);
likelihoodsCl22d = gaussianMV2(meanClass22d,covMatCl22d,twoDTest);
likelihoodsCl32d = gaussianMV2(meanClass32d,covMatCl32d,twoDTest);
likelihoodsCl42d = gaussianMV2(meanClass42d,covMatCl42d,twoDTest);
likelihoodsCl52d = gaussianMV2(meanClass52d,covMatCl52d,twoDTest);
likelihoodsCl62d = gaussianMV2(meanClass62d,covMatCl62d,twoDTest);
likelihoodsCl72d = gaussianMV2(meanClass72d,covMatCl72d,twoDTest);
likelihoodsCl82d = gaussianMV2(meanClass82d,covMatCl82d,twoDTest);
likelihoodsCl92d = gaussianMV2(meanClass92d,covMatCl92d,twoDTest);
likelihoodsCl102d = gaussianMV2(meanClass102d,covMatCl102d,twoDTest);


likelihoodsCl12dlda = gaussianMV2(meanClass12d,covMatShared2d,twoDTest);
likelihoodsCl22dlda = gaussianMV2(meanClass22d,covMatShared2d,twoDTest);
likelihoodsCl32dlda = gaussianMV2(meanClass32d,covMatShared2d,twoDTest);
likelihoodsCl42dlda = gaussianMV2(meanClass42d,covMatShared2d,twoDTest);
likelihoodsCl52dlda = gaussianMV2(meanClass52d,covMatShared2d,twoDTest);
likelihoodsCl62dlda = gaussianMV2(meanClass62d,covMatShared2d,twoDTest);
likelihoodsCl72dlda = gaussianMV2(meanClass72d,covMatShared2d,twoDTest);
likelihoodsCl82dlda = gaussianMV2(meanClass82d,covMatShared2d,twoDTest);
likelihoodsCl92dlda = gaussianMV2(meanClass92d,covMatShared2d,twoDTest);
likelihoodsCl102dlda = gaussianMV2(meanClass102d,covMatShared2d,twoDTest);


allProbsAllPoints2dfullgauss = [likelihoodsCl12d,likelihoodsCl22d,likelihoodsCl32d,likelihoodsCl42d,likelihoodsCl52d,likelihoodsCl62d,likelihoodsCl72d,likelihoodsCl82d,likelihoodsCl92d,likelihoodsCl102d];
[maxProbs2dgauss,indicesMax2dgauss] = max(allProbsAllPoints2dfullgauss,[],2);

confusionGaussian2dfull = computeConf(test_classes,indicesMax2dgauss)
sumCorrectGaussian2dfull = sum(diag(confusionGaussian2dfull))
allClassificationsGaussian2dfull = sum(sum(confusionGaussian2dfull))
accuracyRateGaussian2dfull = sumCorrectGaussian2dfull/allClassificationsGaussian2dfull

allProbsAllPoints2dlda = [likelihoodsCl12dlda,likelihoodsCl22dlda,likelihoodsCl32dlda,likelihoodsCl42dlda,likelihoodsCl52dlda,likelihoodsCl62dlda,likelihoodsCl72dlda,likelihoodsCl82dlda,likelihoodsCl92dlda,likelihoodsCl102dlda];
[maxProbs2dgausslda,indicesMax2dgausslda] = max(allProbsAllPoints2dlda,[],2);

confusionGaussian2dlda = computeConf(test_classes,indicesMax2dgausslda)
sumCorrectGaussian2dlda = sum(diag(confusionGaussian2dlda))
allClassificationsGaussian2dlda = sum(sum(confusionGaussian2dlda))
accuracyRateGaussian2dlda = sumCorrectGaussian2dlda/allClassificationsGaussian2dlda






