function contourGauss2D(mu, covar, col)
    
    if (length(mu)>2)
        disp('error: Only two dimensional data are allowed');
        return
    end
    
    step = 0.1;
    C = covar; % the covariance matrix
    A = inv(C); % the inverse covariance matrix
    
    %Get the variance for each axis
    var = diag(C); 
    
    %Get the maximum variance
    maxsd = max(var(1),var(2));
    
    x = mu(1)-2*maxsd:step:mu(1)+2*maxsd;
    
    [X,Y] = meshgrid(x,y);
    
    % Compute value of Gaussian pdf at each point in the grid
    % writing the quadratic form fully
    
    z = 1/(2*pi*sqrt(det(C)))*exp(-0.5*(A(1,1)*(X-mu(1)).^2+2*A(1,2)*(X-mu(1)).*(Y-mu(2))+A(2,2)*(Y-mu(2).^2)));
    if nargin<3
        contour(x,y,z);
    else
        contour(x,y,z,'Color',col);
    end

    




end