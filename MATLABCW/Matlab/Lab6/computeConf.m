function[confusionMatrix] = computeConf(trueLabels,predictedLabels)
    % Simple confusion matrix function.
    % The function will compute the matrix only for our case, it's not a
    % general function, although I could write one easily.
    
    confusionMatrix = zeros(10,10);
    for count = 1:1000
        confusionMatrix(predictedLabels(count),trueLabels(count)) = confusionMatrix(predictedLabels(count),trueLabels(count)) + 1;
    end
    

end