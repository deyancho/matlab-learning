%Get the unique class labels.
uniqueClassLabels = unique(train_classes');

%(lots of copy-pasting, not a good way to write code but whatever)

%Get the indices for the different classes
class1Indices = (find(train_classes' == uniqueClassLabels(1)));
class2Indices = (find(train_classes' == uniqueClassLabels(2)));
class3Indices = (find(train_classes' == uniqueClassLabels(3)));
class4Indices = (find(train_classes' == uniqueClassLabels(4)));
class5Indices = (find(train_classes' == uniqueClassLabels(5)));
class6Indices = (find(train_classes' == uniqueClassLabels(6)));
class7Indices = (find(train_classes' == uniqueClassLabels(7)));
class8Indices = (find(train_classes' == uniqueClassLabels(8)));
class9Indices = (find(train_classes' == uniqueClassLabels(9)));
class10Indices = (find(train_classes' == uniqueClassLabels(10)));

%Extract the data from the different classes
dataClass1 = train_features(class1Indices,:);
dataClass2 = train_features(class2Indices,:);
dataClass3 = train_features(class3Indices,:);
dataClass4 = train_features(class4Indices,:);
dataClass5 = train_features(class5Indices,:);
dataClass6 = train_features(class6Indices,:);
dataClass7 = train_features(class7Indices,:);
dataClass8 = train_features(class8Indices,:);
dataClass9 = train_features(class9Indices,:);
dataClass10 = train_features(class10Indices,:);

%Get the mean vector for each class
meanClass1 = MyMean(dataClass1);
meanClass2 = MyMean(dataClass2);
meanClass3 = MyMean(dataClass3);
meanClass4 = MyMean(dataClass4);
meanClass5 = MyMean(dataClass5);
meanClass6 = MyMean(dataClass6);
meanClass7 = MyMean(dataClass7);
meanClass8 = MyMean(dataClass8);
meanClass9 = MyMean(dataClass9);
meanClass10 = MyMean(dataClass10);

%The MyCovNotForSample computes the 
%means again which is inefficient, I'll that for now.

covMatCl1 = MyCovNotForSample(dataClass1);
covMatCl2 = MyCovNotForSample(dataClass2);
covMatCl3 = MyCovNotForSample(dataClass3);
covMatCl4 = MyCovNotForSample(dataClass4);
covMatCl5 = MyCovNotForSample(dataClass5);
covMatCl6 = MyCovNotForSample(dataClass6);
covMatCl7 = MyCovNotForSample(dataClass7);
covMatCl8 = MyCovNotForSample(dataClass8);
covMatCl9 = MyCovNotForSample(dataClass9);
covMatCl10 = MyCovNotForSample(dataClass10);

detCovMatCl1 = det(covMatCl1);
detCovMatCl2 = det(covMatCl2);
detCovMatCl3 = det(covMatCl3);
detCovMatCl4 = det(covMatCl4);
detCovMatCl5 = det(covMatCl5);
detCovMatCl6 = det(covMatCl6);
detCovMatCl7 = det(covMatCl7);
detCovMatCl8 = det(covMatCl8);
detCovMatCl9 = det(covMatCl9);
detCovMatCl10 = det(covMatCl10);


likelihoodsCl1 = gaussianMV2(meanClass1,covMatCl1,test_features);
likelihoodsCl2 = gaussianMV2(meanClass2,covMatCl2,test_features);
likelihoodsCl3 = gaussianMV2(meanClass3,covMatCl3,test_features);
likelihoodsCl4 = gaussianMV2(meanClass4,covMatCl4,test_features);
likelihoodsCl5 = gaussianMV2(meanClass5,covMatCl5,test_features);
likelihoodsCl6 = gaussianMV2(meanClass6,covMatCl6,test_features);
likelihoodsCl7 = gaussianMV2(meanClass7,covMatCl7,test_features);
likelihoodsCl8 = gaussianMV2(meanClass8,covMatCl8,test_features);
likelihoodsCl9 = gaussianMV2(meanClass9,covMatCl9,test_features);
likelihoodsCl10 = gaussianMV2(meanClass10,covMatCl10,test_features);


allProbsAllPoints = [likelihoodsCl1,likelihoodsCl2,likelihoodsCl3,likelihoodsCl4,likelihoodsCl5,likelihoodsCl6,likelihoodsCl7,likelihoodsCl8,likelihoodsCl9,likelihoodsCl10];
[maxProbs,indicesMax] = max(allProbsAllPoints,[],2);

confusionGaussian = computeConf(test_classes,indicesMax)
sumCorrectGaussian = sum(diag(confusionGaussian))
allClassificationsGaussian = sum(sum(confusionGaussian))
accuracyRateGaussian = sumCorrectGaussian/allClassificationsGaussian





